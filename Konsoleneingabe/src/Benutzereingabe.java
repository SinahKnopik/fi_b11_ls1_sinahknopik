import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
 
public class Benutzereingabe
{
    public static void main(String[] args) 
    {
	    String vorname ;
	    String nachname;
	    Integer alter;
	    
	    Calendar cal = new GregorianCalendar();
	    int Uhrzeit = cal.get(Calendar.HOUR_OF_DAY); 
	     
	    Scanner a = new Scanner(System.in);
	    System.out.print("Wie lautet Ihr Vorname?");
	    vorname = a.nextLine();
	    
	    
	    Scanner b = new Scanner(System.in);
	    System.out.print("Wie lautet ihr Nachname?");
	    nachname = b.nextLine();
	    
	    Scanner c = new Scanner(System.in);
	    System.out.print("Wie alt sind Sie?");
	    alter = c.nextInt();
	    
	    System.out.println(vorname + " " + nachname + " " + alter);
	  
	    if(Uhrzeit>=0 && Uhrzeit <12)
	    {
	        System.out.print("Guten Morgen "+ vorname + " " + nachname + ".");
	    }
	    else if(Uhrzeit>=12 && Uhrzeit <=18)
	    {
	        System.out.print("Guten Tag "+ vorname + " " + nachname + ".");
	    }
	    else if(Uhrzeit>18 && Uhrzeit <=24)
	    {
	        System.out.print("Guten Abend "+ vorname + " " + nachname + ".");
	    }
        
    }   
}