
import java.util.Scanner;
import java.util.stream.IntStream;

class TicketMachine {
    public static void main(String[] args) {
        double requestedPayment = requestPayment();
        double paidPayment = handlePayment(requestedPayment);
        giveTicket();
        handleChange(requestedPayment - paidPayment);
        showReminder();
    }

    private static double requestPayment() {
        System.out.print("Zu zahlender Betrag (EURO): ");
        return new Scanner(System.in).nextDouble();
    }

    private static void showReminder() {
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir w�nschen Ihnen eine gute Fahrt.");
    }

    private static void handleChange(double change) {
        if (change > 0.0) {
            System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO%n", change);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");
            for (double coin : new double[]{2, 1, 0.5, 0.2, 0.1, 0.05}) {
                change = handleCoins(change, coin);
            }
            assert change == 0;
        }
    }

    private static double handleCoins(double change, double coin) {
        while (change >= coin) {
            System.out.printf("%.2f EURO%n", coin);
            change -= coin;
        }
        return change;
    }

    private static void giveTicket() {
        System.out.println("\nFahrschein wird ausgegeben");
        IntStream.range(0, 8).forEach(i -> {
            System.out.print("=");
            sleep(250);
        });
        System.out.println("\n\n");
    }

    @SuppressWarnings("SameParameterValue")
    private static void sleep(long i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException ignored) {
            //not important
        }
    }

    private static double handlePayment(double requestedPayment) {
        Scanner scanner = new Scanner(System.in);
        double paidPayment = 0;
        while (paidPayment < requestedPayment) {
            System.out.printf("Noch zu zahlen: %.2f%n", requestedPayment - paidPayment);
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            paidPayment += scanner.nextDouble();
        }
        return paidPayment;
    }
}