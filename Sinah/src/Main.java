import java.text.SimpleDateFormat;
import java.util.Date;
public class Main {
	
	public static void main(String[] args) {
		Person p = new Person();
		p.name = "Mustermann";
		p.vorname = "Max";
		p.geschlecht = "m�nnlich";
		p.geburtsjahr = 1987;
		
		p.printPerson();
		
		Person p2 = new Person("Musterfrau", "Anne", "weiblich", 1994);
	
        p2.printPerson();
        
        Person p3 = new Person("M�ller");
        
        p3.printPerson();
        
        p3.vorname = "Klaus";
        
        p3.printPerson();
        
        Person p4 = new Person("Meyer", "Hans", null, 1956);
        
        p4.printPerson();
        		
       
        System.out.println(p.berechneAlter());
        System.out.println(p2.berechneAlter());
        System.out.println(p3.berechneAlter());
        System.out.println(p4.berechneAlter());
       
	}

}
