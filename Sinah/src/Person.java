import java.text.SimpleDateFormat;
import java.util.Date;

public class Person {

	public String name;
	public String vorname;
	public String geschlecht;
	public Integer geburtsjahr;
	
	public Person(){}
	
	public Person(String name, String vorname, String geschlecht, Integer geburtsjahr){
		this.name = name;
		this.vorname = vorname;
		this.geschlecht = geschlecht;
		this.geburtsjahr = geburtsjahr;
		
		}
	
		
	public Person(String name){
		this.name = name;
	}
	
	public String berechneAlter() {
		   if(geburtsjahr == null) {
			   return "Geburtsdatum fehlt.";
		   }
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
	       Date d = new Date();
	       Integer aktuellesjahr = Integer.parseInt(sdf.format(d));
           return Integer.toString(aktuellesjahr - geburtsjahr);
	}
	
	public void printPerson() {
		System.out.println(String.format("Person: %s %s %s %d", name, vorname, geschlecht, geburtsjahr));
	}

	
}


