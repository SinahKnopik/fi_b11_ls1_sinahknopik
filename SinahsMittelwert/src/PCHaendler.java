public class PCHaendler {


    public static void main(String[] args) {

        String name;
        int anzahl;
        double preis;
        double mwst;

        Artikel verkaeufer = new Artikel();

        System.out.println("Was moechten Sie bestellen?");
        name = verkaeufer.getMeineartikel().next();


        System.out.println("Geben Sie die Anzahl ein:");
        anzahl = verkaeufer.getanzahl().nextInt();



        System.out.println("Geben Sie den Nettopreis ein:");
        preis = verkaeufer.getPreis().nextDouble();



        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        mwst = verkaeufer.getMwst().nextDouble();



        double nettogesamtpreis = anzahl * preis ;
        double bruttogesamtpreis = nettogesamtpreis * ( mwst / 100);



        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", name, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", name, anzahl, bruttogesamtpreis, mwst, "%");

    }

}

