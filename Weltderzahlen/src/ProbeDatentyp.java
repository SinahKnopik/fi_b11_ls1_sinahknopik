import java.util.Scanner;
public class ProbeDatentyp {

	public static void main(String[] args) {
    
		Scanner myScanner = new Scanner(System.in);
		
		float zahl1 = 0.0f, zahl2 = 0.0f, summe = zahl1 + zahl2;
		int zahl3 = 0, zahl4 = 0;
		double zahlDouble;
		long zahlLong;
		byte zahlByte;
		short zahlShort;
		boolean Wahrheit;
		char Buchstabe;
	    String name;
		
		
		
		System.out.println("Das Programm addiert zwei Zahlen zusammen.");
		System.out.println("Bitte geben Sie die erste Zahl ein: ");
		zahl1 = myScanner.nextFloat();
		
		System.out.println("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextFloat();
		
		summe = zahl1 + zahl2;
		
		System.out.println(summe);
		
		System.out.println("Bitte geben Sie Ihren Namen ein: ");
		name = myScanner.next();
		System.out.println(name);
		
		System.out.println("Das Programm addiert zwei ganze Zahlen zusammen.");
		System.out.print("Bitte geben Sie die erste Ganzzahl ein: ");
	    zahl3 = myScanner.nextInt();
	    
	    System.out.println("Bitte geben Sie die zweite Ganzzahl ein:");
	    zahl4 = myScanner.nextInt();
	    
		System.out.println("Double:");
		zahlDouble = myScanner.nextDouble();
		
		System.out.println("Long:");
		zahlLong = myScanner.nextLong();
		
		System.out.println("Byte:");
		zahlByte = myScanner.nextByte();
		
		System.out.println("Short:");
		zahlShort = myScanner.nextShort();
		
		System.out.println("Boolean:");
		Wahrheit = myScanner.nextBoolean();
		
		System.out.println("Buchstabe:");
		Buchstabe = myScanner.next().charAt(0);
		
		
		
		
		myScanner.close();
		

	}

}
